#!/bin/bash
ipconfig="$(ipconfig)"
ethernet="$(echo "$ipconfig" | grep -A 9999 Ethernet: | grep -B 9999 -m2 "^ *$")"
something="$(echo "$ethernet" | grep "IPv4 Address" | sed -e 's/.*: \(.*\).*/\1/')"

echo "Start-Process cmd -ArgumentList \"/c\",\"route delete 0.0.0.0 mask 0.0.0.0 ${something}\" -Verb RunAs"
# powershell -Command "Start-Process cmd -ArgumentList \"/c\",\"route delete 0.0.0.0 mask 0.0.0.0 ${something}\" -Verb RunAs"
