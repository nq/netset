#!/usr/bin/env bash
set -x
set -e

abort() {
    echo >&2  '
***************
*** ABORTED ***
***************
'
    echo $1 >&2
    echo "Exiting..." >&2
    exit 1
}

elevate() {
    powershell -Command "Start-Process cmd -ArgumentList  \"/c\",\"$1 | clip\" -Verb RunAs"
    powershell -sta "add-type -as System.Windows.Forms; [windows.forms.clipboard]::GetText()"
}

trap 'abort "Error getting ethernet configuration"' 0
ipconfig="$(ipconfig)"
ethernet=$(echo "$ipconfig" | grep -A 9999 Ethernet: | grep -B 9999 -m2 "^ *$")
trap : 0

if [ "$(echo "$ethernet" | grep -o paychex.com)" != "paychex.com" ]
then
	abort "Ethernet not connected to paychex network"
fi

internal_ip=$(echo "$ethernet" | grep "IPv4 Address" | sed -e 's/.*: \(.*\).*/\1/')
internal_mask=$(echo "$ethernet" | grep "Mask" | sed -e 's/.*: \(.*\).*/\1/')
internal_gateway=$(echo "$ethernet" | grep "Gateway" | sed -e 's/.*: \(.*\).*/\1/')

elevate "reg add HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\NlaSvc\Parameters\Internet /v EnableActiveProbing /t REG_DWORD /d 0 /f"

echo "Connecting to the guest network"
netsh wlan disconnect
netsh wlan connect name=PWGUEST

ipconfig="$(ipconfig)"
wifi=$(echo "$ipconfig" | grep -A 9999 Wi-Fi: | grep -B 9999 -m2 "^ *$")

external_ip=$(echo "$wifi" | grep "IPv4 Address" | sed -e 's/.*: \(.*\).*/\1/')
external_mask=$(echo "$wifi" | grep "Mask" | sed -e 's/.*: \(.*\).*/\1/')
external_gateway=$(echo "$wifi" | grep "Gateway" | sed -e 's/.*: \(.*\).*/\1/')

echo "Removing internal default route"
elevate "route delete 0.0.0.0 mask 0.0.0.0 ${internal_gateway}"

echo "Removing any old static routes"
elevate "route delete 10.10.10.10"

echo "Adding static route for guest network authentication"
elevate "route add 10.10.10.10 mask 255.255.255.255 ${external_gateway}"

netstat -rn

echo "Authenticating with the guest network"
python guest_auth.py

echo "Re-enabling web-base automatic authentication"
elevate "reg add HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\NlaSvc\Parameters\Internet /v EnableActiveProbing /t REG_DWORD /d 1 /f"

echo "Internal ip: $internal_ip"
echo "Internal mask: $internal_mask"
echo "Internal gateway: $internal_gateway"

echo "External ip: $external_ip"
echo "External mask: $external_mask"
echo "External gateway: $external_gateway"


