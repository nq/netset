var page = require('webpage').create();
page.open('https://guestwifi.paychex.com', function(status){
	if (status !== 'success') {
		console.log('Unable to access network');
	} else {
		var ua = page.evaluate(function() {
			var getElementByXpath = function(path) {
  				return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
			}
			getElementByXpath('//*[@id="innershell"]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td[3]/input').value = "wlaw";
			getElementByXpath('//*[@id="innershell"]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/form/table/tbody/tr[2]/td[3]/input').value = "840988";
			getElementByXpath('//*[@id="innershell"]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/form/table/tbody/tr[3]/td[2]/p[3]/input').click();
		});
		console.log(ua);
	}
	phantom.exit();
})
