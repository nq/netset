from selenium import webdriver

page = webdriver.Firefox()
page.get('http://captive.apple.com')
assert 'Paychex Guest Wireless Access' in page.title

username_field = page.find_element_by_name('username')
pw_field = page.find_element_by_name('password')
accept_button = page.find_element_by_name('Submit')

username_field.clear()
username_field.send_keys('wlaw')

pw_field.clear()
pw_field.send_keys('287888')

accept_button.submit()

wait = input("Press enter to continue...")
